export default {
    name: 'OverlayPlayer',
    data () {
        return {
            msg: 'Overlay player'
        }
    },
    computed: {
        show() {
            return this.$store.getters.getOverlayPlayer.show;
        },
        data() {
            return this.$store.getters.getOverlayPlayer.currentEntity;
        },
        index() {
            return this.$store.getters.getOverlayPlayer.currentIndex;
        },
        havePrevious() {
            return this.$store.getters.getOverlayPlayer.currentIndex !== 0;
        },
        haveNext() {
            return this.$store.getters.getOverlayPlayer.currentIndex !== this.$store.getters.newsList.length - 1;
        },
    },
    methods: {
        getEntity(index) {
            return this.$store.getters.newsList[index];
        },
        closeOverlayPlayer() {
            document.getElementsByTagName('body')[0].style.overflow = 'auto';
            this.$store.commit('closeOverlayPlayer');
        },
        goToPrevious () {
            this.$store.commit('changeOverlayPlayer', {
                currentIndex: this.index - 1,
                currentEntity: this.getEntity(this.index - 1)
            });
        },
        goToNext () {
            this.$store.commit('changeOverlayPlayer', {
                currentIndex: this.index + 1,
                currentEntity: this.getEntity(this.index + 1)
            });
        }
    },
    created: function () {
    },
    updated: function () {
    }
}