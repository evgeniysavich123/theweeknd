import {fetchInstagramVideos} from '../../../utils/instagram'

export default {
    name: 'Instagram',
    computed: {
        videos () {
            return this.$store.getters.getInstagramVideos
        },
        hasNextPage () {
            return this.$store.getters.getInstagramPageInfo.hasNextPage
        },
        nextPageToken () {
            return this.$store.getters.getInstagramPageInfo.nextPageToken
        }
    },
    data () {
        return {
            msg: 'Instagram'
        }
    },
    methods: {
        fetchInstagramVideos () {
            let username = 'lazar_angelov_official';
            let pageToken = this.nextPageToken;
            this.$store.dispatch('fetchInstagramVideos', {
                username,
                pageToken
            });
        }
    },
    created: function () {
        this.fetchInstagramVideos();
    }
}