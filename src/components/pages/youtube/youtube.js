export default {
    name: 'YouTube',
    computed: {
        videos () {
            return this.$store.getters.getYouTubeVideos
        },
        hasNextPage () {
            return this.$store.getters.getYouTubePageInfo.hasNextPage
        },
        nextPageToken () {
            return this.$store.getters.getYouTubePageInfo.nextPageToken
        }
    },
    data () {
        return {
            msg: 'YouTube'
        }
    },
    methods: {
        fetchYouTubeVideos () {
            let playlistId = 'PLhCH_nPE4JTpG1YiGV1ke69dqcjuEuEZA';
            let maxResults = 12;
            let pageToken = this.nextPageToken;
            this.$store.dispatch('fetchYouTubeVideos', {
                playlistId,
                maxResults,
                pageToken
            });
        },
        autoPlay(e){
            e.target.src = e.target.src + 'autoplay=1'
        },
        autoPause(e){
            e.target.src = e.target.src.replace('autoplay=1','')
        }
    },
    created: function () {
        this.fetchYouTubeVideos();
    }
}