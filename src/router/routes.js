import AllNews from './../components/allNews/allNews.vue'
import YouTube from '../components/pages/youtube/youtube.vue'
import Instagram from '../components/pages/instagram/instagram.vue'

export const routes = [
    {
        path: '/',
        name: 'AllNews',
        component: AllNews
    },
    {
        path: '/youtube',
        name: 'YouTube',
        component: YouTube
    },
    {
        path: '/instagram',
        name: 'Instagram',
        component: Instagram
    }
];