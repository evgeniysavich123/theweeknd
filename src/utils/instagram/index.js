import axios from 'axios'

const BASE_URL = 'https://www.instagram.com/';

export const fetchInstagramVideos = (username, pageToken = false) => {

    let url = `${username}/?__a=1`;

    if (typeof pageToken === 'string' && pageToken.length) {
        url += `&max_id=${pageToken}`;
    }

    return new Promise((resolve, reject) => {
        axios({
            method: 'get',
            url,
            baseURL: BASE_URL
        }).then(response => {

            let result = {
                success: false,
                data: {
                    videos: [],
                    pageInfo: {
                        hasNextPage: false,
                        nextPageToken: ''
                    }
                }
            };

            if (response.data.user.media.nodes.length) {
                result.success = true;

                for (let node of response.data.user.media.nodes) {

                    let thumbnail = node.thumbnail_resources[node.thumbnail_resources.length - 1];

                    let video = {
                        videoUrl: `https://www.instagram.com/p/${node.code}/`,
                        publishedAt: node.date,
                        img: {
                            url: thumbnail.src,
                            width: thumbnail.config_width,
                            height: thumbnail.config_height
                        }
                    };

                    result.data.videos.push(video);
                }

                if (response.data.user.media.page_info.has_next_page) {
                    result.data.pageInfo.hasNextPage = true;
                    result.data.pageInfo.nextPageToken = response.data.user.media.page_info.end_cursor;
                }
            }

            resolve(result)

        }).catch(error => {
            reject(error)
        })
    })
};