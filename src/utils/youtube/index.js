import axios from 'axios'

const BASE_URL = 'https://www.googleapis.com/youtube/v3/';
const KEY = 'AIzaSyCZTn5XwZ_kVjuJDuzn296rzMqejoEwiOg';

export const fetchYouTubeVideos = (playlistId, maxResults, pageToken = false) => {

    let url = `playlistItems?playlistId=${playlistId}&key=${KEY}&part=snippet&maxResults=${maxResults}`;

    if (typeof pageToken === 'string' && pageToken.length) {
        url += `&pageToken=${pageToken}`;
    }

    return new Promise((resolve, reject) => {
        axios({
            method: 'get',
            url,
            baseURL: BASE_URL
        }).then(response => {
            let result = {
                success: false,
                data: {
                    videos: [],
                    pageInfo: {
                        hasNextPage: false,
                        nextPageToken: ''
                    }
                }
            };

            if (response.data.items.length) {
                result.success = true;

                for (let item of response.data.items) {
                    let video = {
                        videoUrl: `https://www.youtube.com/watch?v=${item.snippet.resourceId.videoId}`,
                        publishedAt: item.snippet.publishedAt,
                        videoFrame: 'https://www.youtube.com/embed/'+ item.snippet.resourceId.videoId + '?rel=0&showinfo=0;controls=0;',
                        img: {
                            url: item.snippet.thumbnails.maxres.url,
                            width: item.snippet.thumbnails.maxres.width,
                            height: item.snippet.thumbnails.maxres.height
                        }
                    };
                    result.data.videos.push(video);
                }

                if (typeof response.data.nextPageToken === 'string' && response.data.nextPageToken.length) {
                    result.data.pageInfo.hasNextPage = true;
                    result.data.pageInfo.nextPageToken = response.data.nextPageToken;
                }
            }

            resolve(result)

        }).catch(error => {
            reject(error)
        })
    })
};