export const getters = {

    getYoutubeNT: state => {
        if (state.youtubeNT) {
            return '&pageToken=' + state.youtubeNT;
        } else {
            return '';
        }
    },
    getInstagramNT: state => {
        if (state.instagramNT) {
            return '&max_id=' + state.instagramNT;
        } else {
            return '';
        }
    },

    getOverlayPlayer: state => {
        return state.overlayPlayer;
    },

    getNewsPage: state => state.newsPage,
    newsList: state => state.newsList,

    doneTodos: state => {
        return state.todos.filter(todo => todo.done)
    },
    doneTodosCount: (state, getters) => {
        return getters.doneTodos.length
    },
    getTodoById: (state, getters) => (id) => {
        return state.todos.find(todo => todo.id === id)
    },
    disabled: state => state.disabled,
    getYouTubeVideos: state => state.youtube.videos,
    getYouTubePageInfo: state => state.youtube.pageInfo,
    getInstagramVideos: state => state.instagram.videos,
    getInstagramPageInfo: state => state.instagram.pageInfo
};