export const mutations = {
    increment: (state, payload) => state.count += payload.amount,
    decrement: state => state.count--,
    ADD_NEW_PROPERTY: (state, payload) => state.person = {...state.person, age: payload.age},
    disabled: (state, payload) => state.disabled = payload.disabled,
    fetchYouTubeVideos: (state, payload) => {
        state.youtube.videos = [...state.youtube.videos, ...payload.videos];
        state.youtube.pageInfo = payload.pageInfo;
    },
    fetchInstagramVideos: (state, payload) => {
        state.instagram.videos = [...state.instagram.videos, ...payload.videos];
        state.instagram.pageInfo = payload.pageInfo;
    },

    fetchNews: (state, payload) => {
        //state.newsList += [...state.newsList, ...payload];
        
        state.newsList = [...state.newsList, ...payload];
        //console.log(state.newsList);
    },

    setYoutubeNT: (state, payload) => {
        state.youtubeNT = payload.youtubeNT;
    },
    setInstagramNT: (state, payload) => {
        state.instagramNT = payload.instagramNT;
    },
    changeOverlayPlayer: (state, payload) => {
        state.overlayPlayer.show = true;
        state.overlayPlayer.currentIndex = payload.currentIndex;
        state.overlayPlayer.currentEntity = payload.currentEntity;
    },
    closeOverlayPlayer: (state) => {
        state.overlayPlayer.show = false;
        state.overlayPlayer.currentIndex = 0;
        state.overlayPlayer.currentEntity = null;
    }

};