export const state = {
    newsPage: 1,
    newsList: [],

    youtubeNT: '',
    instagramNT: '',

    overlayPlayer: {
        currentEntity: null,
        currentIndex: 0,
        show: false
    },

    count: 0,
    disabled: false,
    person: {
        name: 'Barton Curry'
    },
    todos: [
        {
            id: 1,
            text: 'Angular',
            done: true
        },
        {
            id: 2,
            text: 'Vue.js',
            done: false
        },
        {
            id: 3,
            text: 'Node.js',
            done: true
        },
        {
            id: 4,
            text: 'MongoDB',
            done: false
        },
        {
            id: 5,
            text: 'Express',
            done: true
        }
    ],
    youtube: {
        videos: [],
        pageInfo: {
            hasNextPage: false,
            nextPageToken: ''
        }
    },
    instagram: {
        videos: [],
        pageInfo: {
            hasNextPage: false,
            nextPageToken: ''
        }
    }
};