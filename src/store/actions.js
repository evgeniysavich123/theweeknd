import axios from 'axios'

const BASE_URL_YOUTUBE = 'https://www.googleapis.com/youtube/v3/';
const YOUTUBE_KEY = 'AIzaSyCPncNprYeN0mcP-AA-gm5VtDxU8YwRnh4';
const BASE_URL_INSTAGRAM = 'https://www.instagram.com/';

export const actions = {
    fetchNews({commit}, payload) {

        let youtubeData = new Promise((resolve, reject) => {

            let playlistId = 'PLRjIvR8JIRzGMvkGBJfQE-Qb3IxjWSG5M',
                maxResults = 8;

            let url = BASE_URL_YOUTUBE + `playlistItems?playlistId=${playlistId}&key=${YOUTUBE_KEY}&part=snippet&maxResults=${maxResults}` + payload.youtubeNT;

            axios.get(url).then(response => {
                commit('setYoutubeNT', {
                    youtubeNT: response.data.nextPageToken
                });

                let result = [];

                if (response.data.items.length) {
                    result = response.data.items.map(function (item) {
                        return {
                            itemType: 'youtube_video',
                            isVideo: true,
                            publishedAt: new Date(item.snippet.publishedAt),
                            element: 'https://www.youtube.com/embed/'
                            + item.snippet.resourceId.videoId
                            + '?rel=0' +
                            '&showinfo=0' +
                            '&controls=0' +
                            '&loop=1' +
                            '&playlist='
                            + item.snippet.resourceId.videoId,
                            data: {
                                channelTitle: item.snippet.channelTitle,
                                videoId: item.snippet.resourceId.videoId
                            }
                        };
                    });
                }

                resolve(result);
            }).catch(error => {
                reject(error)
            })
        });


        let instagramData = new Promise((resolve, reject) => {

            let username = 'gplama',
                url = BASE_URL_INSTAGRAM + username + '/?__a=1' + payload.instagramNT;

            axios.get(url).then(response => {
                commit('setInstagramNT', {
                    instagramNT: response.data.user.media.page_info.end_cursor
                });

                const USERNAME = `@${response.data.user.username}`;

                const videoElements = response.data.user.media.nodes.filter(e => e.is_video);

                const videoPromises = videoElements.map(item => axios.get(`https://www.instagram.com/p/${item.code}/?__a=1`));

                const videoElements2 = videoElements.map(item => {
                    return {
                        itemType: 'instagram_video',
                        isVideo: item.is_video,
                        publishedAt: new Date(item.date),
                        element: '',
                        data: {
                            username: USERNAME,
                            comments: item.comments.count,
                            likes: item.likes.count
                        }
                    };
                });

                Promise.all(videoPromises).then(resposes => {
                    resposes.forEach((item, index) => {
                        videoElements2[index].element = item.data.graphql.shortcode_media.video_url;
                    })
                });

                const imgElements = response.data.user.media.nodes
                    .filter(e => !e.is_video)
                    .map(item => {
                        return {
                            itemType: 'instagram_image',
                            isVideo: item.is_video,
                            publishedAt: new Date(item.date),
                            element: item.thumbnail_src,
                            data: {
                                username: USERNAME,
                                comments: item.comments.count,
                                likes: item.likes.count
                            }
                        };
                    });

                let result = [...videoElements2, ...imgElements];

                resolve(result);
            }).catch(error => {
                reject(error)
            })
        });

        const sortByDate = (tiles) => {
            return tiles.sort(function (a, b) {
                return b.publishedAt - a.publishedAt;
            });
        };

        Promise.all([youtubeData, instagramData]).then(value => {
            let newArray = [...value[0], ...value[1]];
            let shuffleNews = sortByDate(newArray);
            commit('fetchNews', shuffleNews);
            payload.state.loaded();
        }, reason => {
            console.log(reason);
            payload.state.loaded();
        });
    }
};